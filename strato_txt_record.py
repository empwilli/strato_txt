#!/usr/bin/env python3

import sys
import click

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys

STRATO_URL='https://www.strato.de/apps/CustomerService'

@click.command()
@click.option('--prefix', required=True, default='_acme-challenge', help='The prefix of the record')
@click.option('--username', required=True, envvar='STRATO_USERNAME', help='Strato username')
@click.option('--password', required=True, envvar='STRATO_PASSWORD', help='Strato password')
@click.option('--driver', required=True, envvar='SELENIUM_WEBDRIVER', help='Hostname of the webdriver')
@click.argument('txt_records', nargs=-1)
def cli(prefix, username, password, driver, txt_records):
    '''Update TXT records in Strato using selenium'''
    if not txt_records:
        click.echo('Nothing to do, terminating')
        sys.exit(0)

    driver = webdriver.Remote(command_executor='http://{}:4444/wd/hub'.format(driver),
                              desired_capabilities=DesiredCapabilities.CHROME)

    driver.implicitly_wait(5) # Wait until login and stuff
    driver.get(STRATO_URL)

    # Log in
    user_field = driver.find_element_by_name('identifier')
    pass_field = driver.find_element_by_name('passwd')

    user_field.clear()
    user_field.send_keys(username)
    pass_field.clear()
    pass_field.send_keys(password)
    pass_field.send_keys(Keys.RETURN)

    # Navigate to TXT records
    domains = driver.find_element_by_partial_link_text('Domainverwaltung')
    link_goal = domains.get_attribute('href')
    driver.get(link_goal)
    domain_mgmt = driver.find_element_by_link_text('verwalten')
    domain_mgmt.click()
    elem = driver.find_element_by_xpath("//*[contains(text(), 'TXT Records inklusive SPF und DKIM Einstellungen')]/../div/a[contains(text(), 'verwalten')]")
    driver.get(elem.get_attribute('href'))
    records = driver.find_elements_by_class_name('txt-record-tmpl')

    # See how many TXT records there are around and collect them with the same
    # prefix, since we only update those.
    entries = []
    for record in records:
        prefix_textbox = record.find_element_by_name('prefix')
        if prefix_textbox.text == prefix or prefix.text == '':
            entries.append(record)

    # Create new records in case we don't have enough already
    if len(entries) < len(txt_records):
        add_record = driver.find_element_by_class_name('jss_add_row')
        for _ in range(len(txt_records) - len(entries)):
            driver.execute_script("$(arguments[0]).click();", add_record)
        records = driver.find_elements_by_class_name('txt-record-tmpl')
        for record in records:
            record.find_element_by_name('prefix')
            if record.text == '':
                entries.append(record)

    # Udpate / set key text and prefix to the records
    for key, entry in zip(txt_records, records):
        name = entry.find_element_by_name('prefix')
        name.clear()
        name.send_keys(prefix)
        value = entry.find_element_by_name('value')
        value.clear()
        value.send_keys(key)

    # Finished, send the results
    submit_button = driver.find_element_by_name('action_change_txt_records')
    driver.execute_script("$(arguments[0]).click();", submit_button)
