from setuptools import setup

setup(
    name='strato_txt_record',
    version='0.1',
    py_modules=['strato_txt_record'],
    install_requires=[
        'Click',
        'selenium'
    ],
    entry_points='''
        [console_scripts]
        strato_txt_record=strato_txt_record:cli
    ''',
)
